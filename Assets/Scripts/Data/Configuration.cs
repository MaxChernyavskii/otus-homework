﻿using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "New Configuration", menuName = "Data/Configuration", order = 51)]
    public class Configuration : ScriptableObject
    {
        [SerializeField] private PlayerData _playerData;

        public PlayerData PlayerData => _playerData;
    }
}