﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "New PlayerData", menuName = "Data/PlayerData", order = 51)]
    public class PlayerData : ScriptableObject
    {
        [SerializeField] private string _name;
        [SerializeField] private string _description;
        [SerializeField, PreviewField] private Sprite _logo;

        public string Name => _name;
        public string Description => _description;
        public Sprite Logo => _logo;
    }
}