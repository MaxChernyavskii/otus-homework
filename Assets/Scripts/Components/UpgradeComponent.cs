using System;
using Components.Interfaces;
using Primitives.Events;
using UnityEngine;

namespace Components
{
    public class UpgradeComponent : MonoBehaviour, IUpgradeComponent
    {
        [SerializeField] private EventReceiver _upgradeRequest;
        [SerializeField] private EventReceiver _upgradeCompleted;

        public event Action OnUpgraded
        {
            add => _upgradeCompleted.OnEvent += value;
            remove => _upgradeCompleted.OnEvent -= value;
        }

        public void Upgrade()
        {
            _upgradeRequest.Call();
        }
    }
}