﻿using Components.Interfaces;
using Entities;
using Primitives.Events;
using UnityEngine;

namespace Components
{
    public class AttackComponent : MonoBehaviour, IAttackComponent
    {
        [SerializeField] private EntityEventReceiver _attackTrigger;
        
        public void Attack(IEntity entity)
        {
            _attackTrigger.Call(entity);
        }
    }
}