﻿using System;
using Components.Interfaces;
using Primitives.Events;
using UnityEngine;

namespace Components
{
    public class DeathComponent : MonoBehaviour, IDeathComponent
    {
        [SerializeField] private EventReceiver _deathTrigger;

        public event Action OnDead
        {
            add => _deathTrigger.OnEvent += value;
            remove => _deathTrigger.OnEvent -= value;
        }
        
        public void Die()
        {
            _deathTrigger.Call();
        }
    }
}