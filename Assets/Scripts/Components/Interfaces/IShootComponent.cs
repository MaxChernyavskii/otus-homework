namespace Components.Interfaces
{
    public interface IShootComponent
    {
        void Shoot();
    }
}