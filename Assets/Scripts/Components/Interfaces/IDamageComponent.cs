namespace Components.Interfaces
{
    public interface IDamageComponent
    {
        int Get();
    }
}