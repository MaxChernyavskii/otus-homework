namespace Components.Interfaces
{
    public interface ILevelComponent
    {
        int GetCurrent();
        int GetMax();
    }
}