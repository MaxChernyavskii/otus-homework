namespace Components.Interfaces
{
    public interface IHealthComponent
    {
        int GetCurrent();
        int GetMax();
    }
}