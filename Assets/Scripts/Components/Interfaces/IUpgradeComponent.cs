using System;

namespace Components.Interfaces
{
    public interface IUpgradeComponent
    {
        public event Action OnUpgraded;
        
        void Upgrade();
    }
}