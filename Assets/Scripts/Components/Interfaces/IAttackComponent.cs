﻿using Entities;

namespace Components.Interfaces
{
    public interface IAttackComponent
    {
        void Attack(IEntity entity);
    }
}