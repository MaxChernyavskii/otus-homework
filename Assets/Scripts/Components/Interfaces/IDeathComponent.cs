﻿using System;

namespace Components.Interfaces
{
    public interface IDeathComponent
    {
        event Action OnDead;
        void Die();
    }
}