﻿using Components.Interfaces;
using Primitives.Vectors;
using UnityEngine;

namespace Components
{
    public class MoveComponent : MonoBehaviour, IMoveComponent
    {
        [SerializeField] private Vector3Behaviour _direction;
        
        public void Move(Vector3 direction)
        {
            _direction.Value = direction;
        }
    }
}