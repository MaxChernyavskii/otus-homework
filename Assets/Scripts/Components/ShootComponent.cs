﻿using Components.Interfaces;
using Primitives.Events;
using UnityEngine;

namespace Components
{
    public class ShootComponent : MonoBehaviour, IShootComponent
    {
        [SerializeField] private EventReceiver _shootTrigger;
        
        public void Shoot()
        {
            _shootTrigger.Call();
        }
    }
}