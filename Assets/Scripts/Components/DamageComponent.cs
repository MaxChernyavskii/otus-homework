﻿using Components.Interfaces;
using Primitives.Numbers;
using UnityEngine;

namespace Components
{
    public class DamageComponent : MonoBehaviour, IDamageComponent
    {
        [SerializeField] private IntBehaviour _damage;

        public int Get()
        {
            return _damage.Value;
        }
    }
}