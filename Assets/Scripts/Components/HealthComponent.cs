﻿using Components.Interfaces;
using Primitives.Numbers;
using UnityEngine;

namespace Components
{
    public class HealthComponent : MonoBehaviour, IHealthComponent
    {
        [SerializeField] private IntBehaviour _currentHealth;
        [SerializeField] private IntBehaviour _maxHealth;
        
        public int GetCurrent()
        {
            return _currentHealth.Value;
        }

        public int GetMax()
        {
            return _maxHealth.Value;
        }
    }
}