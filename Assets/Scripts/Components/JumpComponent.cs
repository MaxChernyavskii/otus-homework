﻿using Components.Interfaces;
using Primitives.Events;
using UnityEngine;

namespace Components
{
    public class JumpComponent : MonoBehaviour, IJumpComponent
    {
        [SerializeField] private EventReceiver _jumpTrigger;
        
        public void Jump()
        {
            _jumpTrigger.Call();
        }
    }
}