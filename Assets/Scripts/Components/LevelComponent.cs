﻿using Components.Interfaces;
using Primitives.Numbers;
using UnityEngine;

namespace Components
{
    public class LevelComponent : MonoBehaviour, ILevelComponent
    {
        [SerializeField] private IntBehaviour _currentLevel;
        [SerializeField] private IntBehaviour _maxLevel;
        
        public int GetCurrent()
        {
            return _currentLevel.Value;
        }

        public int GetMax()
        {
            return _maxLevel.Value;
        }
    }
}