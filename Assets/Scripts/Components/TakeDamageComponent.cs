﻿using Components.Interfaces;
using Primitives.Events;
using UnityEngine;

namespace Components
{
    public class TakeDamageComponent : MonoBehaviour, ITakeDamageComponent
    {
        [SerializeField] private IntEventReceiver _damageTrigger;
        
        public void TakeDamage(int value)
        {
            _damageTrigger.Call(value);
        }
    }
}