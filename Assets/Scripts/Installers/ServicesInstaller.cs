﻿using Controllers;
using Data;
using Inputs;
using Services;
using Services.Interfaces;
using UnityEngine;
using View;
using View.Popups;
using View.Presenters;
using Zenject;

namespace Installers
{
    public class ServicesInstaller : MonoInstaller<ServicesInstaller>
    {
        [SerializeField] private Configuration _configuration;
        [SerializeField] private GameContext _gameContext;
        [SerializeField] private PlayerHolder _playerHolder;
        [SerializeField] private MoveInput _moveInput;
        [SerializeField] private JumpInput _jumpInput;
        [SerializeField] private ShootInput _shootInput;
        [SerializeField] private DeathController _deathController;
        [SerializeField] private JumpController _jumpController;
        [SerializeField] private MoveController _moveController;
        [SerializeField] private ShootController _shootController;
        [SerializeField] private PopupController _popupController;
        [SerializeField] private PlayerUpgradePresenterFactory _playerUpgradePresenterFactory;
        [SerializeField] private PlayerUpgradeController _playerUpgradeController;
        [SerializeField] private PlayerUpgradeViewController _playerUpgradeViewController;
        
        public override void InstallBindings()
        {
            Container.BindInstance(_configuration);
            
            Container.BindInstance((IGameContext) _gameContext).AsSingle().NonLazy();
            Container.BindInstance(_playerHolder).AsSingle().NonLazy();

            Container.BindInterfacesAndSelfTo<MoveInput>().FromInstance(_moveInput).NonLazy();
            Container.BindInterfacesAndSelfTo<JumpInput>().FromInstance(_jumpInput).NonLazy();
            Container.BindInterfacesAndSelfTo<ShootInput>().FromInstance(_shootInput).NonLazy();
            
            Container.BindInterfacesAndSelfTo<DeathController>().FromInstance(_deathController).NonLazy();
            Container.BindInterfacesAndSelfTo<JumpController>().FromInstance(_jumpController).NonLazy();
            Container.BindInterfacesAndSelfTo<MoveController>().FromInstance(_moveController).NonLazy();
            Container.BindInterfacesAndSelfTo<ShootController>().FromInstance(_shootController).NonLazy();
            
            Container.BindInterfacesAndSelfTo<PopupController>().FromInstance(_popupController).NonLazy();
            Container.BindInterfacesAndSelfTo<PlayerUpgradePresenterFactory>().FromInstance(_playerUpgradePresenterFactory).NonLazy();
            Container.BindInterfacesAndSelfTo<PlayerUpgradeController>().FromInstance(_playerUpgradeController).NonLazy();
            Container.BindInterfacesAndSelfTo<PlayerUpgradeViewController>().FromInstance(_playerUpgradeViewController).NonLazy();
        }
    }
}