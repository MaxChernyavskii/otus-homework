using Entities;

namespace Primitives.Events
{
    public sealed class EntityEventReceiver : BaseEventReceiver<IEntity> { }
}