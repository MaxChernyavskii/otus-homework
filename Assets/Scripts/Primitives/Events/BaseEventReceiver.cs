using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Primitives.Events
{
    public class BaseEventReceiver<T> : MonoBehaviour
    {
        public event Action<T> OnEvent;
        
        [Button]
        public void Call(T value)
        {
            OnEvent?.Invoke(value);
        }
    }
}