using System;
using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Primitives.Timers
{
    public sealed class TimerBehaviour : MonoBehaviour
    {
        [SerializeField] private float _duration = 3;
        [ShowInInspector, ReadOnly] private float _currentTime;

        private Coroutine _timerCoroutine;
        
        public event Action OnStart;
        public event Action<float> OnChange;
        public event Action OnEnded;

        public bool IsPlaying => _timerCoroutine != null;
        
        public void Play()
        {
            if (_timerCoroutine == null)
            {
                _timerCoroutine = StartCoroutine(TimerRoutine());
            }
        }

        public void Stop()
        {
            if (_timerCoroutine != null)
            {
                StopCoroutine(_timerCoroutine);
                _timerCoroutine = null;
                ResetTime();
            }
        }

        public void ResetTime()
        {
            _currentTime = 0;
        }

        private IEnumerator TimerRoutine()
        {
            OnStart?.Invoke();
            
            while (_currentTime < _duration)
            {
                yield return null;
                _currentTime += Time.deltaTime;
                OnChange?.Invoke(_currentTime);
            }

            _currentTime = _duration;
            _timerCoroutine = null;
            OnEnded?.Invoke();
        }
    }
}