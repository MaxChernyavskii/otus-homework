using System;
using UnityEngine;

namespace Primitives.Numbers
{
    public sealed class IntBehaviour : MonoBehaviour
    {
        [SerializeField] private int _value;
        
        public event Action<int> OnValueChanged;

        public int Value
        {
            get => _value;
            set
            {
                _value = value;
                OnValueChanged?.Invoke(value);
            }
        }
    }
}