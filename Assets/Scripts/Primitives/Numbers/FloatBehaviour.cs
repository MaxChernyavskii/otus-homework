using System;
using UnityEngine;

namespace Primitives.Numbers
{
    public sealed class FloatBehaviour : MonoBehaviour
    {
        [SerializeField] private float _value;

        public event Action<float> OnValueChanged;

        public float Value
        {
            get => _value;
            set
            {
                _value = value;
                OnValueChanged?.Invoke(value);
            }
        }
    }
}