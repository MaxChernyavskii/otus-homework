namespace Primitives.Numbers
{
    public struct LimitedIntValue
    {
        public int Current;
        public int Max;

        public LimitedIntValue(int current, int max)
        {
            Current = current;
            Max = max;
        }
    }
}