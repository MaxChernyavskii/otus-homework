﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Primitives.Vectors
{
    public class Vector3Behaviour : MonoBehaviour
    {
        [SerializeField, OnValueChanged(nameof(TriggerEvent))] 
        private Vector3 _value;

        public event Action<Vector3> OnValueChanged;

        public Vector3 Value
        {
            get => _value;
            set
            {
                _value = value;
                OnValueChanged?.Invoke(_value);
            }
        }

        private void TriggerEvent()
        {
            OnValueChanged?.Invoke(_value);
        }
    }
}