using System;
using Services.Interfaces;
using UnityEngine;

namespace Inputs
{
    public class ShootInput : MonoBehaviour, IGameStarted, IGameFinish
    {
        public event Action OnShoot;

        private void Awake()
        {
            Disable();
        }

        private void Update()
        {
            Handle();
        }

        private void Handle()
        {
            if (Input.GetButtonDown("Fire1"))
            {
                OnShoot?.Invoke();
            }
        }

        void IGameStarted.OnStart()
        {
            Enable();
        }

        void IGameFinish.OnFinish()
        {
            Disable();
        }
        
        private void Enable()
        {
            enabled = true;
        }

        private void Disable()
        {
            enabled = false;
        }
    }
}