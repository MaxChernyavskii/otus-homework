﻿using System;
using Services.Interfaces;
using UnityEngine;

namespace Inputs
{
    public class MoveInput : MonoBehaviour, IGameStarted, IGameFinish
    {
        public event Action<Vector3> OnMove;

        private void Awake()
        {
            Disable();
        }

        private void Update()
        {
            Handle();
        }

        private void Handle()
        {
            Vector3 direction;
            direction.x = Input.GetAxis("Horizontal");
            direction.y = 0f;
            direction.z = Input.GetAxis("Vertical");
            
            OnMove?.Invoke(direction);
        }

        void IGameStarted.OnStart()
        {
            Enable();
        }

        void IGameFinish.OnFinish()
        {
            Disable();
        }
        
        private void Enable()
        {
            enabled = true;
        }

        private void Disable()
        {
            enabled = false;
        }
    }
}