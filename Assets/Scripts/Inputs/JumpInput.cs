using System;
using Services.Interfaces;
using UnityEngine;

namespace Inputs
{
    public class JumpInput : MonoBehaviour, IGameStarted, IGameFinish
    {
        public event Action OnJump;

        private void Awake()
        {
            Disable();
        }

        private void Update()
        {
            Handle();
        }

        private void Handle()
        {
            if (Input.GetButtonDown("Jump"))
            {
                OnJump?.Invoke();
            }
        }
        
        void IGameStarted.OnStart()
        {
            Enable();
        }

        void IGameFinish.OnFinish()
        {
            Disable();
        }
        
        private void Enable()
        {
            enabled = true;
        }

        private void Disable()
        {
            enabled = false;
        }
    }
}