﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Entities
{
    public class EntityComponentsInstaller : MonoBehaviour
    {
        [SerializeField, Required] private Entity _target;
        [SerializeField] private bool _enabled = true;
        [SerializeField, Required] private GameObject _componentsRoot;

        private void Awake()
        {
            if (_enabled == false) return;

            if (_componentsRoot == null || _target == null)
            {
                Debug.LogError("Installer properties is not set");
                return;
            }
            
            RemoveAll();
            Install(_componentsRoot.transform);
        }

        private void RemoveAll()
        {
            var components = _target.GetAll();

            foreach (var component in components)
            {
                _target.Remove(component);
            }
        }

        private void Install(Transform root)
        {
            for (int i = 0; i < root.childCount; i++)
            {
                var components = root.GetChild(i).GetComponents<MonoBehaviour>();

                foreach (var component in components)
                {
                    _target.Add(component);
                }
            }
        }
    }
}