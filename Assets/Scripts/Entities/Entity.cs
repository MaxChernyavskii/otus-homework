﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Entities
{
    public class Entity : MonoBehaviour, IEntity
    {
        [ShowInInspector, ReadOnly]
        private List<object> _components = new();
        
        public T Get<T>()
        {
            var result = FindComponent<T>();
            
            if (result == null)
                throw new KeyNotFoundException($"Component {typeof(T).Name} is not found");

            return result;
        }

        public T[] GetAll<T>()
        {
            var components = new List<T>();
            
            for (int i = 0; i < _components.Count; i++)
            {
                var component = _components[i];

                if (component is T result)
                {
                    components.Add(result);
                }
            }
            
            return components.ToArray();
        }

        public bool TryGet<T>(out T component)
        {
            var result = false;
            component = FindComponent<T>();

            if (component != null)
            {
                result = true;
            }

            return result;
        }

        public object[] GetAll()
        {
            return _components.ToArray();
        }

        public void Add(object component)
        {
            _components.Add(component);
        }

        public void Remove(object component)
        {
            _components.Remove(component);
        }

        private T FindComponent<T>()
        {
            for (int i = 0; i < _components.Count; i++)
            {
                var component = _components[i];

                if (component is T result)
                {
                    return result;
                }
            }

            return default;
        }
    }
}