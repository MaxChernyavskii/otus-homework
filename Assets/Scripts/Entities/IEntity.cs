﻿namespace Entities
{
    public interface IEntity
    {
        T Get<T>();
        
        T[] GetAll<T>();
        
        bool TryGet<T>(out T component);
        
        object[] GetAll();
        
        void Add(object component);
        
        void Remove(object component);
    }
}