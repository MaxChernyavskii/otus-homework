using System.Collections;
using Primitives.Events;
using UnityEngine;

namespace Mechanics
{
    public class FallMechanics : MonoBehaviour
    {
        [SerializeField] private Rigidbody _target;
        [SerializeField] private EventReceiver _deathTrigger;

        private Coroutine _coroutine;
        private float _fallThreshold = 0f;
        
        private void OnEnable()
        {
            StartDetection();
        }

        private void OnDisable()
        {
            StopDetection();
        }

        private void StartDetection()
        {
            _coroutine = StartCoroutine(Detect());
        }

        private IEnumerator Detect()
        {
            var wait = new WaitForSeconds(0.25f);

            while (true)
            {
                if (_target.velocity.y < 0 && _target.worldCenterOfMass.y < _fallThreshold)
                {
                    Debug.Log("Fall detected");
                    _deathTrigger.Call();
                    break;
                }

                yield return wait;
            }
            
            StopDetection();
        }

        private void StopDetection()
        {
            if (_coroutine != null)
            {
                StopCoroutine(_coroutine);
                _coroutine = null;
            }
        }
    }
}