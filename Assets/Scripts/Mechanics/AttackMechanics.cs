using Components.Interfaces;
using Entities;
using Primitives.Events;
using Primitives.Numbers;
using Primitives.Timers;
using UnityEngine;

namespace Mechanics
{
    public sealed class AttackMechanics : MonoBehaviour
    {
        [SerializeField] private EntityEventReceiver _attackRequest;
        [SerializeField] private TimerBehaviour _countdown;
        [SerializeField] private IntBehaviour _damage;

        private void OnEnable()
        {
            _attackRequest.OnEvent += OnRequestAttack;
        }

        private void OnDisable()
        {
            _attackRequest.OnEvent -= OnRequestAttack;
        }

        private void OnRequestAttack(IEntity entity)
        {
            if (_countdown.IsPlaying)
            {
                return;
            }
            
            if (entity.TryGet(out ITakeDamageComponent damageComponent))
            {
                damageComponent.TakeDamage(_damage.Value);
            }

            _countdown.ResetTime();
            _countdown.Play();
        }
    }
}