using Primitives.Events;
using Primitives.Numbers;
using UnityEngine;

namespace Mechanics
{
    public sealed class DeathMechanics : MonoBehaviour
    {
        [SerializeField] private IntBehaviour _health;
        [SerializeField] private EventReceiver _deathReceiver;

        private void OnEnable()
        {
            _health.OnValueChanged += OnHealthChanged;
        }

        private void OnDisable()
        {
            _health.OnValueChanged -= OnHealthChanged;
        }

        private void OnHealthChanged(int newHitPoints)
        {
            if (newHitPoints <= 0)
            {
                _deathReceiver.Call();
            }
        }
    }
}