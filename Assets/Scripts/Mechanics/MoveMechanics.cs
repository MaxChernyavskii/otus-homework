﻿using Primitives.Numbers;
using Primitives.Vectors;
using UnityEngine;

namespace Mechanics
{
    public class MoveMechanics : MonoBehaviour
    {
        [SerializeField] private FloatBehaviour _speed;
        [SerializeField] private Vector3Behaviour _direction;
        [SerializeField] private Transform _target;

        private Rigidbody _targetRigidbody;

        private void OnEnable()
        {
            _targetRigidbody = _target.GetComponent<Rigidbody>();

            if (_targetRigidbody != null)
            {
                _direction.OnValueChanged += OnDirectionChanged;
            }
            else
            {
                Debug.LogWarning("Target hasn't rigidbody component");
            }
        }

        private void OnDisable()
        {
            _direction.OnValueChanged -= OnDirectionChanged;
        }

        private void OnDirectionChanged(Vector3 direction)
        {
            if (_target == null)
            {
                Debug.LogWarning($"[{gameObject.name}] - Target for movement is not set");
                return;
            }

            var newPosition = _targetRigidbody.position + _direction.Value * _speed.Value * Time.deltaTime;
            Move(newPosition);
        }

        private void Move(Vector3 position)
        {
            _targetRigidbody.MovePosition(position);
        }
    }
}