﻿using Engines;
using Primitives.Events;
using UnityEngine;

namespace Mechanics
{
    public class ShootMechanics : MonoBehaviour
    {
        [SerializeField] private EventReceiver _shoot;
        [SerializeField] private ShootProjectileEngine _projectileEngine;

        private void OnEnable()
        {
            _shoot.OnEvent += Shoot;
        }

        private void OnDisable()
        {
            _shoot.OnEvent -= Shoot;
        }

        private void Shoot()
        { 
            _projectileEngine.Shoot();   
        }
    }
}