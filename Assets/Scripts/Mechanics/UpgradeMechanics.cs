using Primitives.Events;
using Primitives.Numbers;
using UnityEngine;

namespace Mechanics
{
    public class UpgradeMechanics : MonoBehaviour
    {
        [SerializeField] private EventReceiver _upgradeRequest;
        [SerializeField] private EventReceiver _upgradeCompleted;
        [SerializeField] private IntBehaviour _currentLevel;
        [SerializeField] private IntBehaviour _maxLevel;
        [SerializeField] private IntBehaviour _currentHealth;
        [SerializeField] private IntBehaviour _maxHealth;
        [SerializeField] private IntBehaviour _damage;

        private void OnEnable()
        {
            _upgradeRequest.OnEvent += OnRequest;
        }
        
        private void OnDisable()
        {
            _upgradeRequest.OnEvent -= OnRequest;
        }

        private void OnRequest()
        {
            if (IsUpgradeAvailable() == false) return;

            if (TryUpgrade())
            {
                Debug.Log("Upgrade Completed");
                _upgradeCompleted.Call();
            }
        }

        private bool IsUpgradeAvailable()
        {
            return true;
        }

        private bool TryUpgrade()
        {
            if (_currentLevel.Value >= _maxLevel.Value)
            {
                Debug.LogWarning("Reached max level!");
                return false;
            }

            _currentLevel.Value += 1;
            _maxHealth.Value += 1;
            _currentHealth.Value = _maxHealth.Value;
            _damage.Value += 1;

            return true;
        }
    }
}