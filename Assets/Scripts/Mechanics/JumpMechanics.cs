﻿using Primitives.Events;
using Primitives.Numbers;
using UnityEngine;

namespace Mechanics
{
    public class JumpMechanics : MonoBehaviour
    {
        [SerializeField] private Rigidbody _target;
        [SerializeField] private EventReceiver _jump;
        [SerializeField] private FloatBehaviour _force;

        private void OnEnable()
        {
            _jump.OnEvent += Jump;
        }

        private void OnDisable()
        {
            _jump.OnEvent -= Jump;
        }

        private void Jump()
        {
            _target.velocity += new Vector3(0f, _force.Value, 0f);
        }
    }
}