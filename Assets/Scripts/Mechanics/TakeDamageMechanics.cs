using Primitives.Events;
using Primitives.Numbers;
using UnityEngine;

namespace Mechanics
{
    public sealed class TakeDamageMechanics : MonoBehaviour
    {
        [SerializeField] private IntEventReceiver _takeDamageReceiver;
        [SerializeField] private IntBehaviour _health;

        private void OnEnable()
        {
            _takeDamageReceiver.OnEvent += OnDamageTaken;
        }

        private void OnDisable()
        {
            _takeDamageReceiver.OnEvent -= OnDamageTaken;
        }

        private void OnDamageTaken(int damage)
        {
            _health.Value -= damage;
        }
    }
}