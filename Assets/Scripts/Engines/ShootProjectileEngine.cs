using Primitives.Numbers;
using UnityEngine;

namespace Engines
{
    public class ShootProjectileEngine : MonoBehaviour
    {
        [SerializeField] private Projectile _projectilePrefab;
        [SerializeField] private FloatBehaviour _force;
        [SerializeField] private Transform _direction;
        [SerializeField] private Transform _projectileSpawnPoint;
        
        public void Shoot()
        {
            var position = _projectileSpawnPoint.position;
            var forward = _direction.forward;
            var rotation = Quaternion.LookRotation(forward);
            var projectile = Instantiate(_projectilePrefab, position, rotation);
            projectile.transform.forward = forward;
            projectile.Fly(_force.Value);
        }
    }
}