﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private Rigidbody _rigidbody;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    public void Fly(float force)
    {
        _rigidbody.velocity += transform.forward * force;
    }
}