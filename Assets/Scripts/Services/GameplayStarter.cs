﻿using System;
using Primitives.Timers;
using Services.Interfaces;
using UnityEngine;
using Zenject;

namespace Services
{
    public class GameplayStarter : MonoBehaviour
    {
        [SerializeField] private TimerBehaviour _timer;

        private IGameContext _gameContext;

        public event Action OnStartCountdown
        {
            add => _timer.OnStart += value;
            remove => _timer.OnStart -= value;
        }
        
        public event Action<float> OnChangeCountdown
        {
            add => _timer.OnChange += value;
            remove => _timer.OnChange -= value;
        }
        
        public event Action OnEndedCountdown
        {
            add => _timer.OnEnded += value;
            remove => _timer.OnEnded -= value;
        }

        [Inject]
        private void Construct(IGameContext gameContext)
        {
            _gameContext = gameContext;
        }
        
        private void OnEnable()
        {
            OnEndedCountdown += OnTimerEnded;
        }
        
        private void OnDisable()
        {
            OnEndedCountdown -= OnTimerEnded;
        }

        private void Start()
        {
            StartTimer();
        }

        private void StartTimer()
        {
            _timer.Play();
        }
        
        private void OnTimerEnded()
        {
            _gameContext.StartGame();
        }
    }
}