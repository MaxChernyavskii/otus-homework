﻿using System;
using System.Collections.Generic;
using Services.Interfaces;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

namespace Services
{
    public class GameContext : MonoBehaviour, IGameContext
    {
        public event Action OnGameStarted;
        public event Action OnGameFinished;

        private IGameInit[] _gameInitListeners;
        private List<IGameStarted> _gameStartedListeners;
        private List<IGameFinish> _gameFinishListeners;

        [Inject]
        public void SetupInitListeners(IGameInit[] gameInitListeners)
        {
            _gameInitListeners = gameInitListeners;
        }
        
        [Inject]
        public void SetupStartListeners(IGameStarted[] gameStartedListeners)
        {
            _gameStartedListeners = new List<IGameStarted>(gameStartedListeners);
        }
        
        [Inject]
        public void SetupFinishListeners(IGameFinish[] gameFinishListeners)
        {
            _gameFinishListeners = new List<IGameFinish>(gameFinishListeners);
        }

        private void Start()
        {
            InitGame();
        }

        [Button]
        public void InitGame()
        {
            foreach (var gameInit in _gameInitListeners)
            {
                gameInit.OnInit();
            }

            Debug.Log("Game initialized!");
        }

        [Button]
        public void StartGame()
        {
            foreach (var gameStarted in _gameStartedListeners)
            {
                gameStarted.OnStart();
            }
            
            OnGameStarted?.Invoke();

            Debug.Log("Game Started!");
        }

        [Button]
        public void FinishGame()
        {
            foreach (var gameFinish in _gameFinishListeners)
            {
                gameFinish.OnFinish();
            }
            
            OnGameFinished?.Invoke();
            
            Debug.Log("Game Finished!");
        }

        public void AddStartListener(IGameStarted gameStarted)
        {
            _gameStartedListeners.Add(gameStarted);
        }

        public void RemoveStartListener(IGameStarted gameStarted)
        {
            _gameStartedListeners.Remove(gameStarted);
        }

        public void AddFinishListener(IGameFinish gameFinish)
        {
            _gameFinishListeners.Add(gameFinish);
        }

        public void RemoveFinishListener(IGameFinish gameFinish)
        {
            _gameFinishListeners.Remove(gameFinish);
        }
    }
}