﻿using Entities;
using UnityEngine;

namespace Services
{
    public class PlayerHolder : MonoBehaviour
    {
        [SerializeField] private Entity _player;

        public Entity Get()
        {
            return _player;
        }
    }
}