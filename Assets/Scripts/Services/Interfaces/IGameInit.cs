﻿namespace Services.Interfaces
{
    public interface IGameInit
    {
        void OnInit();
    }
}