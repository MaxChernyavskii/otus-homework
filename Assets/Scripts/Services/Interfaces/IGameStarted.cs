namespace Services.Interfaces
{
    public interface IGameStarted
    {
        void OnStart();
    }
}