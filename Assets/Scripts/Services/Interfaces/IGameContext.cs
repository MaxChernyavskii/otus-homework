using System;

namespace Services.Interfaces
{
    public interface IGameContext
    {
        event Action OnGameStarted;

        event Action OnGameFinished;

        void StartGame();
    
        void FinishGame();
        void AddStartListener(IGameStarted gameStarted);
        
        void RemoveStartListener(IGameStarted gameStarted);
        
        void AddFinishListener(IGameFinish gameFinish);
        
        void RemoveFinishListener(IGameFinish gameFinish);
    }
}