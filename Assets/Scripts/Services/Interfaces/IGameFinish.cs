namespace Services.Interfaces
{
    public interface IGameFinish
    {
        void OnFinish();
    }
}