using System;
using Components.Interfaces;
using Entities;
using Services;
using Services.Interfaces;
using UnityEngine;
using Zenject;

namespace Controllers
{
    public class PlayerUpgradeController : MonoBehaviour, IGameInit
    {
        private IUpgradeComponent _upgradeComponent;
        private Entity _player;

        public event Action OnUpgraded
        {
            add => _upgradeComponent.OnUpgraded += value;
            remove => _upgradeComponent.OnUpgraded -= value;
        }

        [Inject]
        private void Construct(PlayerHolder playerHolder)
        {
            _player = playerHolder.Get();
        }

        public void OnInit()
        {
            _upgradeComponent = _player.Get<IUpgradeComponent>();
        }

        public void Upgrade()
        {
            _upgradeComponent.Upgrade();
        }
    }
}