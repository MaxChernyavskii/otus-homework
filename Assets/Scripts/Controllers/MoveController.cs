using Components.Interfaces;
using Entities;
using Inputs;
using Services;
using Services.Interfaces;
using UnityEngine;
using Zenject;

namespace Controllers
{
    public class MoveController : MonoBehaviour, IGameInit, IGameStarted, IGameFinish
    {
        private MoveInput _input;
        private Entity _target;
        private IMoveComponent _moveComponent;
        private PlayerHolder _playerHolder;
        
        [Inject]
        private void Construct(PlayerHolder playerHolder, MoveInput moveInput)
        {
            _playerHolder = playerHolder;
            _input = moveInput;
        }

        void IGameInit.OnInit()
        {
            _target = _playerHolder.Get();
            _moveComponent = _target.Get<IMoveComponent>();
        }

        void IGameStarted.OnStart()
        {
            _input.OnMove += Move;
        }

        void IGameFinish.OnFinish()
        {
            _input.OnMove -= Move;
        }

        private void Move(Vector3 direction)
        {
            _moveComponent.Move(direction);
        }
    }
}