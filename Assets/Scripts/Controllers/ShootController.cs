using Components.Interfaces;
using Entities;
using Inputs;
using Services;
using Services.Interfaces;
using UnityEngine;
using Zenject;

namespace Controllers
{
    public class ShootController : MonoBehaviour, IGameInit, IGameStarted, IGameFinish
    {
        private ShootInput _input;
        private Entity _target;
        private IShootComponent _shootComponent;
        private PlayerHolder _playerHolder;
        
        [Inject]
        private void Construct(PlayerHolder playerHolder, ShootInput shootInput)
        {
            _playerHolder = playerHolder;
            _input = shootInput;
        }

        void IGameInit.OnInit()
        {
            _target = _playerHolder.Get();
            _shootComponent = _target.Get<IShootComponent>();
        }

        void IGameStarted.OnStart()
        {
            _input.OnShoot += Shoot;
        }

        void IGameFinish.OnFinish()
        {
            _input.OnShoot -= Shoot;
        }

        private void Shoot()
        {
            _shootComponent.Shoot();
        }
    }
}