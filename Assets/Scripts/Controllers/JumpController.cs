using Components.Interfaces;
using Entities;
using Inputs;
using Services;
using Services.Interfaces;
using UnityEngine;
using Zenject;

namespace Controllers
{
    public class JumpController : MonoBehaviour, IGameInit, IGameStarted, IGameFinish
    {
        private JumpInput _input;
        private Entity _target;
        private IJumpComponent _jumpComponent;
        private PlayerHolder _playerHolder;

        [Inject]
        private void Construct(PlayerHolder playerHolder, JumpInput jumpInput)
        {
            _playerHolder = playerHolder;
            _input = jumpInput;
        }

        void IGameInit.OnInit()
        {
            _target = _playerHolder.Get();
            _jumpComponent = _target.Get<IJumpComponent>();
        }

        void IGameStarted.OnStart()
        {
            _input.OnJump += Jump;
        }

        void IGameFinish.OnFinish()
        {
            _input.OnJump -= Jump;
        }

        private void Jump()
        {
            _jumpComponent.Jump();
        }
    }
}