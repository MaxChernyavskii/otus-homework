﻿using Components.Interfaces;
using Services;
using Services.Interfaces;
using UnityEngine;
using Zenject;

namespace Controllers
{
    public class DeathController : MonoBehaviour, IGameInit, IGameStarted, IGameFinish
    {
        private IGameContext _gameContext;
        private PlayerHolder _playerHolder;
        private IDeathComponent _deathComponent;

        [Inject]
        private void Construct(IGameContext gameContext, PlayerHolder playerHolder)
        {
            _gameContext = gameContext;
            _playerHolder = playerHolder;
        }

        void IGameInit.OnInit()
        {
            var player = _playerHolder.Get();
            _deathComponent = player.Get<IDeathComponent>();
        }

        void IGameStarted.OnStart()
        {
            _deathComponent.OnDead += OnDead;
        }

        void IGameFinish.OnFinish()
        {
            _deathComponent.OnDead -= OnDead;
        }

        private void OnDead()
        {
            _gameContext.FinishGame();
        }
    }
}