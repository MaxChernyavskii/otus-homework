﻿using TMPro;
using UnityEngine;

namespace View
{
    public class LimitedView : MonoBehaviour
    {
        [SerializeField] private TMP_Text _title;
        [SerializeField] private TMP_Text _current;
        [SerializeField] private TMP_Text _max;

        public void SetAll(string titleValue, string currentValue, string maxValue)
        {
            SetTitle(titleValue);
            SetCurrentValue(currentValue);
            SetMaxValue(maxValue);
        }
        
        public void SetTitle(string value)
        {
            Set(_title, value);
        }

        public void SetCurrentValue(string value)
        {
            Set(_current, value);
        }
        
        public void SetMaxValue(string value)
        {
            Set(_max, value);
        }

        private void Set(TMP_Text target, string value)
        {
            target.text = value;
        }
    }
}