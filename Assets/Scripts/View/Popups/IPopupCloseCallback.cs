namespace View.Popups
{
    public interface IPopupCloseCallback
    {
        void OnClose(Popup popup);
    }
}