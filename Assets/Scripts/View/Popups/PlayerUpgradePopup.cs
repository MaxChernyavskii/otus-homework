using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using View.Presenters.Interfaces;

namespace View.Popups
{
    public class PlayerUpgradePopup : Popup
    {
        [SerializeField] private TMP_Text _title;
        [SerializeField] private TMP_Text _playerName;
        [SerializeField] private TMP_Text _playerDescription;
        [SerializeField] private Image _icon;
        [SerializeField] private LimitedView _level;
        [SerializeField] private LimitedView _health;
        [SerializeField] private TMP_Text _damageTitle;
        [SerializeField] private TMP_Text _damage;
        [SerializeField] private Button _levelUpButton;

        private IPlayerUpgradePopupPresenter _presenter;

        protected override void OnShow(object args)
        {
            if (args is not IPlayerUpgradePopupPresenter presenter)
            {
                throw new ArgumentException($"Expected args of {nameof(IPlayerUpgradePopupPresenter)} type");
            }
            
            _presenter = presenter;
            _presenter.OnStatsChanged += OnStateChanged;
            _levelUpButton.onClick.AddListener(OnLevelUpClicked);
            
            UpdateState(presenter);
        }

        private void OnStateChanged()
        {
            UpdateState(_presenter);
        }

        private void UpdateState(IPlayerUpgradePopupPresenter presenter)
        {
            _title.text = presenter.GetTitle();
            _playerName.text = presenter.GetPlayerName();
            _playerDescription.text = presenter.GetDescription();
            _icon.sprite = presenter.GetIcon();
            
            var levelData = presenter.GetLevelData();
            var healthData = presenter.GetHealthData();
            
            _level.SetAll("Level:", levelData.Current.ToString(), levelData.Max.ToString());
            _health.SetAll("Health:", healthData.Current.ToString(), healthData.Max.ToString());
            _damageTitle.text = "Damage:";
            _damage.text = presenter.GetDamage().ToString();
        }

        private void OnLevelUpClicked()
        {
            _presenter.OnLevelUpClicked();
        }

        protected override void OnHide()
        {
            _levelUpButton.onClick.RemoveListener(OnLevelUpClicked);
            if (_presenter != null)
                _presenter.OnStatsChanged -= OnStateChanged;
        }
    }
}