﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace View.Popups
{
    public class PopupController : MonoBehaviour, IPopupCloseCallback
    {
        [SerializeField] private List<PopupItem> _popups;

        private readonly Dictionary<PopupName, Popup> _activePopups = new();

        private void Awake()
        {
            foreach (var popupItem in _popups)
            {
                popupItem.Popup.Hide();
            }
        }

        [Button]
        public void Show(PopupName popupName, object args = null)
        {
            if (IsActive(popupName))
            {
                Debug.LogWarning($"Popup {popupName} is already active");
                return;
            }

            var popup = GetPopup(popupName);

            if (popup == null)
            {
                throw new KeyNotFoundException($"Popup {popupName} is not found");
            }
            
            popup.Show(args, this);
            _activePopups.Add(popupName, popup);
        }

        [Button]
        public void Hide(PopupName popupName)
        {
            if (TryGetActive(popupName, out Popup popup))
            {
                popup.Hide();
                _activePopups.Remove(popupName);
            }
            else
            {
                Debug.LogWarning($"Popup {popupName} is not active");
            }
        }

        void IPopupCloseCallback.OnClose(Popup popup)
        {
            var popupName = GetPopupName(popup);
            Hide(popupName);
        }

        private bool IsActive(PopupName popupName)
        {
            return _activePopups.ContainsKey(popupName);
        }

        private bool TryGetActive(PopupName popupName, out Popup popup)
        {
            var result = false;
            popup = null;
            
            if (IsActive(popupName))
            {
                popup = _activePopups[popupName];
                result = true;
            }

            return result;
        }

        private Popup GetPopup(PopupName popupName)
        {
            foreach (var popupItem in _popups)
            {
                if (popupItem.Name == popupName)
                    return popupItem.Popup;
            }

            return null;
        }

        private PopupName GetPopupName(Popup popup)
        {
            foreach (var popupItem in _popups)
            {
                if (popupItem.Popup == popup)
                    return popupItem.Name;
            }

            throw new KeyNotFoundException("Popup name is not found");
        }
    }
}