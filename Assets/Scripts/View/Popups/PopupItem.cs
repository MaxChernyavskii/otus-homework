﻿using System;

namespace View.Popups
{
    [Serializable]
    public struct PopupItem
    {
        public PopupName Name;
        public Popup Popup;
    }
}