﻿using UnityEngine;

namespace View.Popups
{
    public class Popup : MonoBehaviour
    {
        [SerializeField] private GameObject _view;
        
        private IPopupCloseCallback _popupCloseCallback;

        public void Show(object args = null, IPopupCloseCallback popupCloseCallback = null)
        {
            _view.SetActive(true);
            _popupCloseCallback = popupCloseCallback;
            OnShow(args);
        }

        public void Hide()
        {
            _view.SetActive(false);
            OnHide();
        }

        public void RequestClose()
        {
            _popupCloseCallback?.OnClose(this);
        }

        protected virtual void OnShow(object args) { }

        protected virtual void OnHide() { }
    }
}