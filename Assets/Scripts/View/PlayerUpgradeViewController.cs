using Services.Interfaces;
using Sirenix.OdinInspector;
using UnityEngine;
using View.Popups;
using View.Presenters;
using View.Presenters.Interfaces;
using Zenject;

namespace View
{
    public class PlayerUpgradeViewController : MonoBehaviour, IGameInit
    {
        private PopupController _popupController;
        private PlayerUpgradePresenterFactory _presenterFactory;
        private IPlayerUpgradePopupPresenter _presenter;
        
        [Inject]
        private void Construct(PopupController popupController, PlayerUpgradePresenterFactory presenterFactory)
        {
            _popupController = popupController;
            _presenterFactory = presenterFactory;
        }

        public void OnInit()
        {
            _presenter = _presenterFactory.CreatePresenter();
        }

        [Button]
        public void ShowPopup()
        {
            _popupController.Show(PopupName.PlayerUpgrade, _presenter);
        }

        [Button]
        public void HidePopup()
        {
            _popupController.Hide(PopupName.PlayerUpgrade);
        }
    }
}