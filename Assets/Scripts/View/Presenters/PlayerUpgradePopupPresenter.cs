using System;
using Components.Interfaces;
using Controllers;
using Data;
using Primitives.Numbers;
using Services;
using UnityEngine;
using View.Presenters.Interfaces;

namespace View.Presenters
{
    public class PlayerUpgradePopupPresenter : IPlayerUpgradePopupPresenter
    {
        private readonly PlayerUpgradeController _playerUpgradeController;
        private readonly PlayerData _playerData;
        private readonly ILevelComponent _levelComponent;
        private readonly IHealthComponent _healthComponent;
        private readonly IDamageComponent _damageComponent;
        private string _title = "Upgrade Player";
        
        public event Action OnStatsChanged
        {
            add => _playerUpgradeController.OnUpgraded += value;
            remove => _playerUpgradeController.OnUpgraded -= value;
        }

        public PlayerUpgradePopupPresenter(PlayerHolder playerHolder, PlayerUpgradeController playerUpgradeController, PlayerData playerData)
        {
            var player = playerHolder.Get();
             _levelComponent = player.Get<ILevelComponent>();
             _healthComponent = player.Get<IHealthComponent>();
             _damageComponent = player.Get<IDamageComponent>();
            _playerUpgradeController = playerUpgradeController;
            _playerData = playerData;
        }
        
        public string GetTitle()
        {
            return _title;
        }

        public string GetPlayerName()
        {
            return _playerData.Name;
        }

        public string GetDescription()
        {
            return _playerData.Description;
        }

        public Sprite GetIcon()
        {
            return _playerData.Logo;
        }

        public LimitedIntValue GetLevelData()
        {
            return new LimitedIntValue(_levelComponent.GetCurrent(), _levelComponent.GetMax());
        }

        public LimitedIntValue GetHealthData()
        {
            return new LimitedIntValue(_healthComponent.GetCurrent(), _healthComponent.GetMax());
        }

        public int GetDamage()
        {
            return _damageComponent.Get();
        }

        public void OnLevelUpClicked()
        {
            Debug.Log("OnLevelUpClicked");
            _playerUpgradeController.Upgrade();
        }
    }
}