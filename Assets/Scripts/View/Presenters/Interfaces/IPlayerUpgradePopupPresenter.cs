using System;
using Primitives.Numbers;
using UnityEngine;

namespace View.Presenters.Interfaces
{
    public interface IPlayerUpgradePopupPresenter
    {
        event Action OnStatsChanged;
        
        string GetTitle();
        
        string GetPlayerName();

        string GetDescription();

        Sprite GetIcon();

        LimitedIntValue GetLevelData();
        
        LimitedIntValue GetHealthData();
        
        int GetDamage();
        
        void OnLevelUpClicked();
    }
}