using Controllers;
using Data;
using Services;
using UnityEngine;
using View.Presenters.Interfaces;
using Zenject;

namespace View.Presenters
{
    public class PlayerUpgradePresenterFactory : MonoBehaviour
    {
        private PlayerUpgradeController _playerUpgradeController;
        private PlayerHolder _playerHolder;
        private PlayerData _playerData;
        
        [Inject]
        private void Construct(PlayerHolder playerHolder, PlayerUpgradeController playerUpgradeController, Configuration configuration)
        {
            _playerHolder = playerHolder;
            _playerUpgradeController = playerUpgradeController;
            _playerData = configuration.PlayerData;
        }
        
        public IPlayerUpgradePopupPresenter CreatePresenter()
        {
            return new PlayerUpgradePopupPresenter(_playerHolder, _playerUpgradeController, _playerData);
        }
    }
}